#!/bin/bash
set -ex

# download the firmware packages
wget -q ${QCOM_LINUX_FIRMWARE}
echo "${QCOM_LINUX_FIRMWARE_MD5}  $(basename ${QCOM_LINUX_FIRMWARE})" > MD5
md5sum -c MD5

unzip -j -d bootloaders-linux $(basename ${QCOM_LINUX_FIRMWARE}) "*/bootloaders-linux/*" "*/cdt-linux/*" "*/loaders/*" "*/efs-seed/*"

# Get the Android compiler
git clone ${LK_GCC_GIT} --depth 1 -b ${LK_GCC_REL} android-gcc

# get the signing tools
git clone --depth 1 ${QCOM_SIGNLK_GIT}

# Build all needed flavors of LK
git clone --depth 1 ${LK_GIT_LINARO} -b ${LK_GIT_REL_SD_RESCUE} lk_sdrescue
git clone --depth 1 ${LK_GIT_LINARO} -b ${LK_GIT_REL_SD_BOOT} lk_sd_boot
git clone --depth 1 ${LK_GIT_LINARO} -b ${LK_GIT_REL_EMMC_BOOT} lk_emmc_boot

for lk in lk_sdrescue lk_sd_boot lk_emmc_boot; do
    echo "Building LK in : $lk"
    cd $lk
    git log -1
    make -j4 msm8916 EMMC_BOOT=1 TOOLCHAIN_PREFIX=${CI_PROJECT_DIR}/android-gcc/bin/arm-eabi-
    mv build-msm8916/emmc_appsboot.mbn build-msm8916/emmc_appsboot_unsigned.mbn
    ../signlk/signlk.sh -i=./build-msm8916/emmc_appsboot_unsigned.mbn -o=./build-msm8916/emmc_appsboot.mbn -d
    cd -
done

BOOTLOADER_SD_LINUX=dragonboard-410c-bootloader-sd-linux-${CI_PIPELINE_ID}
BOOTLOADER_EMMC_LINUX=dragonboard-410c-bootloader-emmc-linux-${CI_PIPELINE_ID}
BOOTLOADER_EMMC_AOSP=dragonboard-410c-bootloader-emmc-aosp-${CI_PIPELINE_ID}

mkdir -p out/${BOOTLOADER_SD_LINUX} \
      out/${BOOTLOADER_EMMC_LINUX} \
      out/${BOOTLOADER_EMMC_AOSP}

# get LICENSE file (for Linux BSP)
unzip -j $(basename ${QCOM_LINUX_FIRMWARE}) "*/LICENSE"
echo "${QCOM_LINUX_FIRMWARE_LICENSE_MD5}  LICENSE" > MD5
md5sum -c MD5

# Create ptable and rawprogram/patch command files
git clone --depth 1 ${QCOM_PTOOL_GIT} ${CI_PROJECT_DIR}/ptool

(cd ${CI_PROJECT_DIR}/ptool && git log -1)
(mkdir ${CI_PROJECT_DIR}/ptool/linux && cd ${CI_PROJECT_DIR}/ptool/linux && python2 ${CI_PROJECT_DIR}/ptool/ptool.py -x ${CI_PROJECT_DIR}/dragonboard410c/linux/partition.xml)
(mkdir ${CI_PROJECT_DIR}/ptool/aosp && cd ${CI_PROJECT_DIR}/ptool/aosp && python2 ${CI_PROJECT_DIR}/ptool/ptool.py -x ${CI_PROJECT_DIR}/dragonboard410c/aosp/partition.xml)

# Empty/zero boot image file to clear boot partition
dd if=/dev/zero of=boot-erase.img bs=1024 count=1024

# bootloader_emmc_linux
cp -a LICENSE \
   dragonboard410c/linux/flashall \
   lk_emmc_boot/build-msm8916/emmc_appsboot.mbn \
   bootloaders-linux/prog_emmc_firehose_8916.mbn \
   bootloaders-linux/{NON-HLOS.bin,rpm.mbn,sbl1.mbn,tz.mbn,hyp.mbn,sbc_1.0_8016.bin} \
   bootloaders-linux/fs_image_linux.tar.gz.mbn.img \
   ptool/linux/{rawprogram,patch}?.xml \
   ptool/linux/gpt_{main,backup,both}?.bin \
   ptool/linux/zeros_*.bin \
   boot-erase.img \
   out/${BOOTLOADER_EMMC_LINUX}

# bootloader_emmc_aosp
cp -a LICENSE \
   dragonboard410c/aosp/flashall \
   lk_emmc_boot/build-msm8916/emmc_appsboot.mbn \
   bootloaders-linux/{NON-HLOS.bin,rpm.mbn,sbl1.mbn,tz.mbn,hyp.mbn,sbc_1.0_8016.bin} \
   bootloaders-linux/fs_image_linux.tar.gz.mbn.img \
   ptool/aosp/{rawprogram,patch}?.xml \
   ptool/aosp/gpt_{main,backup,both}?.bin \
   ptool/aosp/zeros_*.bin \
   boot-erase.img \
   out/${BOOTLOADER_EMMC_AOSP}

# bootloader_sd_linux
cp -a LICENSE \
   lk_sd_boot/build-msm8916/emmc_appsboot.mbn \
   bootloaders-linux/{NON-HLOS.bin,rpm.mbn,sbl1.mbn,tz.mbn,hyp.mbn,sbc_1.0_8016.bin} \
   bootloaders-linux/fs_image_linux.tar.gz.mbn.img \
   out/${BOOTLOADER_SD_LINUX}

# Final preparation of archives for publishing
mkdir ${CI_PROJECT_DIR}/out2
for i in ${BOOTLOADER_SD_LINUX} \
         ${BOOTLOADER_EMMC_LINUX} \
         ${BOOTLOADER_EMMC_AOSP} ; do
    (cd out/$i && md5sum * > MD5SUMS.txt)
    (cd out && zip -r ${CI_PROJECT_DIR}/out2/$i.zip $i)
done

# Create MD5SUMS file
(cd ${CI_PROJECT_DIR}/out2 && md5sum * > MD5SUMS.txt)

# Build information
cat > ${CI_PROJECT_DIR}/out2/HEADER.textile << EOF

h4. Bootloaders for Dragonboard 410c

This page provides the bootloaders packages for the Dragonboard 410c. There are several packages:
* *bootloader-emmc-linux* : includes the bootloaders and partition table (GPT) used when booting Linux images from onboard eMMC
* *bootloader-emmc-aosp* : includes the bootloaders and partition table (GPT) used when booting AOSP based images from onboard eMMC
* *bootloader-sd-linux* : includes the bootloaders and partition table (GPT) used when booting Linux images from SD card

Build description:
* Build URL: "$CI_JOB_URL":$CI_JOB_URL
* Proprietary bootloaders can be found on "Qualcomm Developer Network":https://developer.qualcomm.com/hardware/dragonboard-410c/tools
* Linux proprietary bootloaders package: $(basename ${QCOM_LINUX_FIRMWARE})
* Little Kernel (LK) source code:
** "SD rescue boot":${LK_GIT_LINARO%.git}/-/tree/$(echo $LK_GIT_REL_SD_RESCUE  | sed -e 's/+/\%2b/g')
** "SD Linux boot":${LK_GIT_LINARO%.git}/-/tree/$(echo $LK_GIT_REL_SD_BOOT | sed -e 's/+/\%2b/g')
** "eMMC Linux boot":${LK_GIT_LINARO%.git}/-/tree/$(echo $LK_GIT_REL_EMMC_BOOT | sed -e 's/+/\%2b/g')
* Tools version: "$DB_BOOT_TOOLS_COMMIT":${DB_BOOT_TOOLS_GIT%.git}/-/commit/$DB_BOOT_TOOLS_COMMIT
* Partition table:
** "Linux":${DB_BOOT_TOOLS_GIT%.git}/-/blob/$DB_BOOT_TOOLS_COMMIT/dragonboard410c/linux/partition.xml
** "AOSP":${DB_BOOT_TOOLS_GIT%.git}/-/blob/$DB_BOOT_TOOLS_COMMIT/dragonboard410c/aosp/partition.xml
EOF

# Publish based on PUBLISH_ARTIFACTS variable
if [[ "$PUBLISH_ARTIFACTS" == "false" ]]; then
    exit
fi

time python3 $(which linaro-cp.py) \
     --server ${PUBLISH_SERVER} \
     --link-latest \
     ${CI_PROJECT_DIR}/out2 ${PUB_DEST}
