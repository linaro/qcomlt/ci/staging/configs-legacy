#!/bin/bash

# Generate Debian Package from kernel checkouted at CWD
#
# Input:
# - CWD: checkouted kernel git tree
# - $CI_PROJECT_DIR: should be CWD
# - $CI_JOB_ID: CLO CI Job ID
# - $ARCH: kernel architecture to build (ARM or ARM64)
# - $KERNEL_CONFIG: kernel defconfig name
#
# Output:
# - out/${ARCH}/release.env

set -ex

# Build variables
KERNEL_COMMIT="$(git rev-parse HEAD)"
KERNEL_VERSION="$(make kernelversion)"
KERNEL_COMMIT_SHORT="$(git rev-parse --short HEAD)"

# SRCVERSION is the main kernel version, e.g. <version>.<patchlevel>.0.
SRCVERSION=$(echo ${KERNEL_VERSION} | sed 's/\(.*\)\..*/\1.0/')
PKGVERSION="${KERNEL_VERSION}-g${KERNEL_COMMIT_SHORT}"

# Debian package identifiers
export KERNELRELEASE="${SRCVERSION}-qcomlt-${ARCH}"
export KDEB_SOURCENAME="linux-${SRCVERSION}-qcomlt-${ARCH}"
export KDEB_PKGVERSION="${PKGVERSION}-${CI_JOB_ID}"

# Cleanup
[ -e ../$(basename $PWD).orig ] && rm -fr ../$(basename $PWD).orig

# Create tarball from current source
git archive --format=tar.gz --prefix=${KDEB_SOURCENAME}-${PKGVERSION}/ -o ../${KDEB_SOURCENAME}_${PKGVERSION}.orig.tar.gz HEAD

# Make config, use distro.config if available
if [ -e kernel/configs/distro.config ]; then
    make ${KERNEL_CONFIG} distro.config
    make savedefconfig
    mv defconfig arch/${ARCH}/configs/${KERNEL_CONFIG}
else
    make ${KERNEL_CONFIG}
fi
make clean

# Generate debian directory
KBUILD_DEBARCH=${ARCH} KCONFIG_CONFIG=.config MAKE=make ./scripts/package/mkdebian

# Up to 6.2, kernel uses format 1.0, since 6.3 format 3.0 is used
FMT=$(cat debian/source/format)
if [[ "$FMT" = "3.0"* ]]; then
    dpkg-source --before-build .
    dpkg-source --build .
else
    dpkg-source --diff-ignore=.git --extend-diff-ignore=scripts -sp --build .
fi

for file in ../${KDEB_SOURCENAME}_${PKGVERSION}*; do 
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $file "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${KDEB_SOURCENAME}/${PKGVERSION}/$(basename $file)?select=package_file"
done

# Clean up
rm -fr ../${KDEB_SOURCENAME}* ../$(basename $PWD).orig

mkdir -p out/${ARCH}

# Write variable to env file with filename and public URL
cat > out/${ARCH}/release.env << EOF
KERNELRELEASE="$KERNELRELEASE"
KDEB_SOURCENAME="$KDEB_SOURCENAME"
KDEB_SOURCEVERSION="$PKGVERSION"
KDEB_PKGVERSION="$KDEB_PKGVERSION"
KDEB_DSC_FILE="${KDEB_SOURCENAME}_${KDEB_PKGVERSION}.dsc"
KDEB_ARTIFACTS_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${KDEB_SOURCENAME}/${PKGVERSION}/"
EOF

echo "Build success, parameters:"
cat out/${ARCH}/release.env
