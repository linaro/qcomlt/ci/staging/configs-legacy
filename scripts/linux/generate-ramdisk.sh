#!/bin/sh

# Customizes a ramdisk with the kernel modules used for LAVA tests
#
# Input:
# - $CI_PROJECT_DIR: base dir of CI build
# - $CI_PROJECT_DIR/out/kernel/parameters.env: should be a sourceable file containing the kernel build variables
#
# Output:
# - $CI_PROJECT_DIR/out/ramdisk.env

set -ex

wget_error() {
    wget --quiet --timeout=60 --progress=dot -c $1
    retcode=$?
    if [ ${retcode} -ne 0 ]; then
        exit ${retcode}
    fi
}

# Get kernel parameters (KERNEL_MODULES_PATH)
source ${CI_PROJECT_DIR}/out/kernel/parameters.env

export GZ=pigz

# Clone QCOMLT ci job configs
[ -d $CI_PROJECT_DIR/configs ] || git clone --depth 1 http://git.linaro.org/ci/job/configs.git

# find ramdisk to use, TODO set as job variable
./configs/lt-qcom-linux-test/get_latest_testimage.py ${TESTIMAGE_URL}

RAMDISK_OUT_FILE="${CI_PROJECT_DIR}/out/ramdisk.cpio.gz"

RAMDISK_URL=$(cat output.log  | grep RAMDISK_URL | cut -d= -f2)

wget_error ${RAMDISK_URL}
ramdisk_file=$(basename ${RAMDISK_URL})
ramdisk_basename=$(basename ${ramdisk_file} .cpio.gz)

if [[ "${ramdisk_basename}" = "${ramdisk_file}" ]]; then
    echo "ERROR: ${ramdisk_file} type should be .cpio.gz"
    exit 1
fi

# Convert modules.tar.xz to modules.cpio.gz to append it to ramdisk
mkdir modules
tar -xJf ${KERNEL_MODULES_PATH} -C modules/
(cd modules; find . | cpio -R 0:0 -oV -H newc > ${CI_PROJECT_DIR}/modules.cpio)
rm -fr modules
${GZ} ${CI_PROJECT_DIR}/modules.cpio
modules_file="${CI_PROJECT_DIR}/modules.cpio.gz"

# Get LAVA test-definitions and copy only needed tests/files
LAVA_TESTS="usb-smoke meminfo hci-smoke wlan-smoke lmbench crypto"
git clone --depth 1 https://github.com/Linaro/test-definitions.git
mkdir -p out/test-definitions
mkdir -p out/test-definitions/automated/lib/
mkdir -p out/test-definitions/automated/utils/
mkdir -p out/test-definitions/automated/linux/
cp test-definitions/automated/utils/send-to-lava.sh out/test-definitions/automated/utils/
cp test-definitions/automated/lib/* out/test-definitions/automated/lib/
for TEST in ${LAVA_TESTS}; do
    cp -ar test-definitions/automated/linux/${TEST} out/test-definitions/automated/linux/
done
(cd out; find test-definitions/ | cpio -R 0:0 -oV -H newc > ${CI_PROJECT_DIR}/test-definitions.cpio)
rm -fr test-definitions out/test-definitions
${GZ} ${CI_PROJECT_DIR}/test-definitions.cpio
test_definitions_file="${CI_PROJECT_DIR}/test-definitions.cpio.gz"

# append the module & test defitions to the ramdisk file
cat ${ramdisk_file} ${modules_file} ${test_definitions_file} > ${RAMDISK_OUT_FILE}

curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${RAMDISK_OUT_FILE} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bootimg-${CI_PIPELINE_ID}/ramdisk/ramdisk.cpio.gz"

# Write variable to env file with filename and public URL
cat > out/${MACHINE}/ramdisk.env << EOF
RAMDISK_FILE="ramdisk.cpio.gz"
RAMDISK_ARTIFACTS_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bootimg-${CI_PIPELINE_ID}/ramdisk/"
EOF

# Cleanup
rm ${ramdisk_file} ${modules_file} ${RAMDISK_OUT_FILE}
