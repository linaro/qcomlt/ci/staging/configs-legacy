#!/bin/sh

# Emits JUnit XML from a SQUAD build testjobs JSON
#
# Input:
# - test_jobs.json: SQUAD build testjobs JSON
#
# Output:
# - JUnit XML to stdout

JOBS_COUNT=$(cat test_jobs.json | jq -r '.results | length')

TMPFILE=`mktemp`

echo "<testsuites><testsuite>"

if [ $JOBS_COUNT -gt 0 ]; then
	for job in $(seq 0  $(($JOBS_COUNT - 1))); do
		JOB=$(cat test_jobs.json | jq -r ".results | .[$job]")
		BOARD=$(echo $JOB | jq -r '.environment')
		TESTRUN_URL=$(echo $JOB | jq -r '.testrun')

		TESTRUN=$(curl -s -o- $TESTRUN_URL)
		TESTS_URL=$(echo $TESTRUN | jq -r '.tests')

		while [ ! "$TESTS_URL" = "null" ]; do
			curl -s -o $TMPFILE $TESTS_URL
			TESTS_COUNT=$(cat $TMPFILE | jq -r '.results | length')

			if [ "$TESTS_COUNT" -gt 0 ]; then
				for tests in $(seq 0  $(($TESTS_COUNT - 1))); do
					NAME=$(cat $TMPFILE | jq -r ".results | .[$tests] | .short_name")
					STATUS=$(cat $TMPFILE | jq -r ".results | .[$tests] | .status")
					LOG=$(cat $TMPFILE | jq -r ".results | .[$tests] | .log")

					echo "<testcase classname=\"$BOARD\" name=\"$NAME\">"

					if [ ! "$STATUS" = "pass" ]; then
						echo "<failure type=\"failure\"><![CDATA[$LOG]]></failure>"
					fi
					echo "</testcase>"
				done
			fi

			TESTS_URL=$(cat $TMPFILE | jq -r '.next')
		done	
	done
fi
echo "</testsuite></testsuites>"

rm $TMPFILE
