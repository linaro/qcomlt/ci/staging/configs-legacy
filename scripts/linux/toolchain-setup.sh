# Setup toolchain for Linux kernel build
# The toolchain can be cached as the toolchain.tar.xz file
# If the file isn't cached, download it

toolchain_url=${TOOLCHAIN_URL}

tcdir=$CI_PROJECT_DIR/toolchain
mkdir -p "${tcdir}"

tcarchive="toolchain.tar.xz"
if [ ! -e "${tcarchive}" ]; then
    wget "${toolchain_url}" -O "${tcarchive}"
fi

tcbindir="${tcdir}/$(basename $toolchain_url .tar.xz)/bin"
tar -xf "${tcarchive}" -C "${tcdir}"
# tcbindir from install-gcc-toolchain.sh
toolchain="$(basename $(ls -1 ${tcbindir}/*-gcc))"

export PATH=$tcbindir:$PATH
export CROSS_COMPILE="ccache $(basename $toolchain gcc)"
export KERNEL_TOOLCHAIN="$(ccache $toolchain --version | head -1)"
