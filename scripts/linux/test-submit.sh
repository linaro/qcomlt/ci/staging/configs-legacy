#!/bin/sh

# Submit all LAVA jobs to SQUAD
#
# Input:
# - $CI_PROJECT_DIR: base dir of CI build
# - $CI_PROJECT_DIR/out/${MACHINE}/lava/*.yaml: LAVA test definitions

set -ex

MACHINES=$(cd out && ls)

echo Machines: $MACHINES

# Submit all LAVA jobs to SQUAD with ${CI_PIPELINE_ID} as build identifier
for MACHINE in $MACHINES; do
    echo "Submit for $MACHINE"

    if [[ -d "${CI_PROJECT_DIR}/out/${MACHINE}/lava/" ]]; then
        for TEST in ${CI_PROJECT_DIR}/out/${MACHINE}/lava/*.yaml; do
            if [[ -f ${TEST} ]]; then
                echo "Submit $(basename ${TEST}) for $MACHINE"
                curl ${QA_SERVER}/api/submitjob/${QA_SERVER_GROUP}/${QA_SERVER_PROJECT}/${CI_PIPELINE_ID}/${MACHINE} --header "Auth-Token: ${CI_SQUAD_TOKEN}"  --form "backend=ovss-lava" --form "definition=@${TEST}"
            fi
        done
    else
        echo "Skipping ${MACHINE}, no lava yaml directory"
    fi
done

# Attach a callback to this build to trigger the lava-report job
squad_build_id="$(curl --silent "$QA_SERVER/api/builds/?version=$CI_PIPELINE_ID" | jq -r '.results[0].id')"
curl --silent \
    -X POST "$QA_SERVER/api/builds/$squad_build_id/callbacks/" \
    -H "Authorization: Token $CI_SQUAD_TOKEN" \
    -F "callback_url=$CALLBACK_URL" \
    -F "callback_headers={\"PRIVATE-TOKEN\": \"$CI_API_TOKEN\"}" \
    -F "callback_record_response=true"
