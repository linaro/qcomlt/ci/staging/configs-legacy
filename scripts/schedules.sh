#!/usr/bin/env bash

set -ex

# globals
CI_API_V4_URL="https://git.codelinaro.org/api/v4"
PROJECT_PATH="${PROJECT_PATH:-linaro/qcomlt/ci/configs}"
PROJECT_ID="${PROJECT_PATH//\//%2F}"

set_pipeline_schedule () {
    local config="$1"
    local cron="$2"

    # set the scheduled pipeline
    post_request="$(curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
         --form description="$config" \
         --form ref="master" \
         --form cron="$cron" \
         --form cron_timezone="UTC" \
         --form active="true" \
         "${CI_API_V4_URL}/projects/${PROJECT_ID}/pipeline_schedules")"

    echo "Scheduled Pipeline Created"
    echo "$post_request" | jq .

    # set the CONFIGS_PIPELINE variable to the new scheduled pipeline
    PIPELINE_ID="$(echo "$post_request" | jq .id)"
    post_request="$(curl -s --request POST \
         --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
         --form "key=CONFIGS_PIPELINE" \
         --form "value=$config" \
         "${CI_API_V4_URL}/projects/${PROJECT_ID}/pipeline_schedules/$PIPELINE_ID/variables")"

    echo "CONFIGS_PIPELINE variable set in scheduled pipeline $PIPELINE_ID"
    echo "$post_request" | jq .
}

# weekly crons
weekly_cron='0 0 * * 0'
weekly_polls=""

# daily crons
daily_cron='0 0 * * *'
daily_polls="oe-master-poll"

# hourly crons
hourly_cron='0 * * * *'
hourly_polls="linux-automerge-poll oe-dunfell-poll oe-honister-poll"

for config in $weekly_polls; do set_pipeline_schedule "$config" "$weekly_cron"; done
for config in $daily_polls;  do set_pipeline_schedule "$config" "$daily_cron";  done
for config in $hourly_polls; do set_pipeline_schedule "$config" "$hourly_cron"; done
