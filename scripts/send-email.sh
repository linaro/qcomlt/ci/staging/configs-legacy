#!/usr/bin/env bash

set -ex

# do not send email when SEND_EMAIL=false
if [[ "${SEND_EMAIL}" == "false" ]]; then
    exit 0
fi

cat <<EOF> ~/.msmtprc
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile        ~/.msmtp.log
account        linaro
host           smtp.gmail.com
port           587
from           $USER_EMAIL
user           $USER_EMAIL
password       $SMTP_PASS
account default : linaro
EOF

chmod 600 ~/.msmtprc

# CI_JOB_STATUS is not set correctly (see [1]) at CLO so we introduced
# the following logic: if $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS file is
# present, this means that job completed SUCCCESFULLY, otherwise
# there was a failure before completetion
# [1] https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27693
CI_JOB_STATUS=''

# String variable indicating the 'pipeline name' on the email status
# if pipeline succesfully completes, it is set to CONFIGS_PIPELINE,
# otherwise (fails) to the corresponding job's name CI_JOB_NAME
CI_NOTIFY_SUBJECT_ID=''

# String variable indicating the Job's URL in case of failure
CI_NOTIFY_BODY_JOB_ID=''

if [ -f "$CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS" ]; then
    CI_JOB_STATUS='Success'
    CI_NOTIFY_SUBJECT_ID=$CONFIGS_PIPELINE
else
    CI_JOB_STATUS='Failure'
    CI_NOTIFY_SUBJECT_ID=$CI_JOB_NAME
    CI_NOTIFY_BODY_JOB_ID="Job URL: $CI_JOB_URL"
fi

# this script can be called from an external repo, where the CONFIGS_PIPELINE
# variable is never set/considered, so just set the CI_JOB_NAME as default
if [ -z "$CI_NOTIFY_SUBJECT_ID" ]; then
    CI_NOTIFY_SUBJECT_ID=$CI_JOB_NAME
fi

if [ ! -f $CI_PROJECT_DIR/header.txt ]; then
    cat<<EOF> $CI_PROJECT_DIR/header.txt
To: $USER_EMAIL
From: $USER_EMAIL
Subject: [CI-NOTIFY] [$CI_NOTIFY_SUBJECT_ID]: Pipeline $CI_PIPELINE_ID: ${CI_JOB_STATUS}!
EOF
fi

# create a basic email body, in case not already populated
if [ ! -f $CI_PROJECT_DIR/email.txt ]; then
    cat<<EOF>$CI_PROJECT_DIR/email.txt

Pipeline URL: ${CI_PIPELINE_URL}
${CI_NOTIFY_BODY_JOB_ID}
EOF
fi

cat $CI_PROJECT_DIR/header.txt $CI_PROJECT_DIR/email.txt | msmtp $TO_EMAILS
