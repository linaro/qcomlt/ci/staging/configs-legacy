set -ex

# create output directory
BOOTIMAGE_OUT_DIR="$BOOTIMAGE_CACHE/$(basename $DTB)"
mkdir -p $BOOTIMAGE_OUT_DIR

# Create boot image for each DTB
# format is 'dtb_file,rootfs_label', if rootfs_label is missing, default to 'rootfs'
BOOTIMG_PAGESIZE='4096'
BOOTIMG_BASE='0x80000000'
BOOTIMG_KERNEL_OFFSET='0x8000'
BOOTIMG_RAMDISK_OFFSET='0x1000000'
BOOTIMG_TAGS_OFFSET='0x100'

dtb_file=$(echo ${DTB} | cut -f1 -d';')
rootfs_label=$(echo ${DTB} | cut -f2 -d';')
dtb_name=$(basename $(echo ${dtb_file} | tr '/' '_' ) .dtb)

if [ "${rootfs_label}" == "" ]; then
    rootfs_label='rootfs'
fi

KERNEL_CMDLINE="root=PARTLABEL=${rootfs_label} console=tty0 console=${SERIAL_CONSOLE},115200n8 ${KERNEL_CMDLINE_PLATFORM}"

BOOTIMG=boot-${VENDOR}-${OS_FLAVOUR}-${PLATFORM_NAME}-${dtb_name}-${CI_PIPELINE_ID}.img

# pick first fai (ROOTFS-ROOTFS_SZ) folder from fai-cache
FAI_OUT_DIR="$(ls -d $FAI_CACHE/*/ | head -1)"
FAI_OUT_DIR="${FAI_OUT_DIR%/}"

cp $FAI_OUT_DIR/dtbs/${dtb_file} $BOOTIMAGE_OUT_DIR/${dtb_name}.dtb
cat $FAI_OUT_DIR/vmlinuz-* $BOOTIMAGE_OUT_DIR/${dtb_name}.dtb > Image.gz+dtb

mkbootimg \
    --kernel Image.gz+dtb \
    --ramdisk $FAI_OUT_DIR/initrd.img-* \
    --output $BOOTIMAGE_OUT_DIR/${BOOTIMG} \
    --pagesize "${BOOTIMG_PAGESIZE}" \
    --base "${BOOTIMG_BASE}" \
    --kernel_offset "${BOOTIMG_KERNEL_OFFSET}" \
    --ramdisk_offset "${BOOTIMG_RAMDISK_OFFSET}" \
    --tags_offset "${BOOTIMG_TAGS_OFFSET}" \
    --cmdline "${KERNEL_CMDLINE}"

pigz -9 $BOOTIMAGE_OUT_DIR/${BOOTIMG}
