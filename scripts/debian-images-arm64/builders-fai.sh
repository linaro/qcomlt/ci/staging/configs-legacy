set -ex

# Build information
mkdir -p out
cat > out/HEADER.textile << EOF

h4. QCOM Landing Team -  #${CI_PIPELINE_ID}

Build description:
* Build URL: "$CI_JOB_URL":$CI_JOB_URL
* OS flavour: $OS_FLAVOUR
* FAI: "$CI_PROJECT_URL":$CI_PROJECT_URL
* FAI commit: "$CI_COMMIT_SHA":$CI_PROJECT_URL/-/commit/$CI_COMMIT_SHA
EOF

# dumb utility to parse dpkg -l output
wget https://git.linaro.org/ci/job/configs.git/blob_plain/HEAD:/lt-qcom-debian-images/debpkgdiff.py

mknod /dev/loop0 b 7 0

CLASS=$(echo SAVECACHE,${OS_FLAVOUR},DEBIAN,LINARO,QCOM,${ROOTFS},RAW | tr '[:lower:]' '[:upper:]')
OUT=${VENDOR}-${OS_FLAVOUR}-${ROOTFS}-${PLATFORM_NAME}-${CI_PIPELINE_ID}

fai-diskimage -v --cspace $(pwd) --hostname linaro-${ROOTFS} -S ${ROOTFS_SZ} --class ${CLASS} ${OUT}.img.raw

sudo cp /var/log/fai/linaro-${ROOTFS}/last/fai.log out/fai-${ROOTFS}.log

if grep -E '^(ERROR:|WARNING: These unknown packages are removed from the installation list|Exit code task_)' out/fai-${ROOTFS}.log
then
    echo "Errors during build"
    rm -rf out/*
    exit 1
fi

rootfs_sz_real=$(du -h ${OUT}.img.raw | cut -f1)

img2simg ${OUT}.img.raw out/${OUT}.img
pigz -9 out/${OUT}.img

# dpkg -l output
mv out/packages.txt out/${OUT}.packages

# record changes since last build, if available
if wget -q ${PUBLISH_SERVER}$(dirname ${PUB_DEST})/latest/${VENDOR}-${OS_FLAVOUR}-${ROOTFS}-${PLATFORM_NAME}-*.packages -O last-build.packages; then
    echo -e "=== Packages changes for ${OUT}\n" >> out/build-changes.txt
    python3 debpkgdiff.py last-build.packages out/${OUT}.packages >> out/build-changes.txt
    echo >> out/build-changes.txt
else
    echo "latest build published does not have packages list, skipping diff report"
fi

# record list of installed packages in git
cp out/${OUT}.packages lt-ci/${VENDOR}-${OS_FLAVOUR}-${ROOTFS}.packages

cat >> out/HEADER.textile << EOF
* Linaro Debian ${rootfs}: size: ${rootfs_sz_real}
EOF

# Record info about kernel, there can be multiple .packages files, but we have already checked that kernel version is the same. so pick one.
kernel_binpkg=$(grep -h linux-image out/${VENDOR}-${OS_FLAVOUR}-*-${PLATFORM_NAME}-${CI_PIPELINE_ID}.packages | sed 's/\s\s*/ /g' | cut -d ' ' -f2 | uniq)
kernel_pkgver=$(grep -h linux-image out/${VENDOR}-${OS_FLAVOUR}-*-${PLATFORM_NAME}-${CI_PIPELINE_ID}.packages | sed 's/\s\s*/ /g' | cut -d ' ' -f3 | uniq)

# record kernel config changes since last build, if available
if wget -q ${PUBLISH_SERVER}$(dirname ${PUB_DEST})/latest/config-* -O last-build.config; then
    echo -e "=== Changes for kernel config\n" >> out/build-changes.txt
    diff -su last-build.config out/config-* >> out/build-changes.txt || true
    echo >> out/build-changes.txt
else
    echo "latest build published does not have kernel config, skipping diff report"
fi

# record kernel config changes in git
cp out/config-* lt-ci/config

# the space after pre.. tag is on purpose
if [ -f out/build-changes.txt ]; then
    cat > out/README.textile << EOF

h4. Build changes

pre.. 
EOF
    cat out/build-changes.txt >> out/README.textile
else
    cat > out/README.textile << EOF

h4. No build changes
EOF
fi

cat >> out/HEADER.textile << EOF
* Kernel package name: ${kernel_binpkg}
* Kernel package version: ${kernel_pkgver}
EOF

# copy out dir into $FAI_CACHE/$ROOTFS-$ROOTFS_SZ/out
# so a later stage can check md5sums
mkdir -p    $FAI_CACHE/$ROOTFS-$ROOTFS_SZ
cp -a out/* $FAI_CACHE/$ROOTFS-$ROOTFS_SZ
