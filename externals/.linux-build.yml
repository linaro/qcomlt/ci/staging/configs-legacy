variables:
  # TODO all this should be taken from a config file
  ARM_TOOLCHAIN: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-arm-none-eabi.tar.xz'
  ARM64_TOOLCHAIN: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz'
  LAVA_TEST_PLANS_GIT_REPO: "https://staging-git.codelinaro.org/linaro/landing-teams/qcomlt/migration/private/neilarmstrong/ci/lava-test-plans.git"
  LAVA_TEST_PLANS_GIT_BRANCH: "oe-rpb-initramfs-test"
  TESTIMAGE_URL: "https://snapshots.linaro.org/member-builds/qcomlt/testimages/qcom-armv8a/"
  PIPELINE_TYPE: ''

stages:
  - build
  - generate
  - test
  - report

# Empty template to filter Linux jobs
.linux-jobs:
  rules:

# Only enable LAVA jobs if QA_SERVER is declared
.lava-jobs:
  rules:
    - if: $PIPELINE_TYPE == 'linux' && $QA_SERVER =~ /^./
    - if: $PIPELINE_TYPE == '' && $QA_SERVER =~ /^./

# Linux build template with ccache
# The variables should be defined in the job definition:
#  - ARCH: arm64/arm/...
#  - KERNEL_CONFIG: defconfig/multi_v7_defconfig/...
#  - TOOLCHAIN_URL: https://url/to/toolchain.tar.xz
# The toolchain URL is also used a key for the toolchain CI job cache
.linux-build:
  interruptible: true
  variables: !reference [.linux_resources, variables]
  stage: build
  before_script:
    - export PATH="/usr/lib/ccache:$PATH"
    - export CCACHE_DIR="$PWD/ccache"
    - export CCACHE_COMPILERCHECK=content
    - ccache --zero-stats || true
    - ccache --show-stats || true
    - source <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/toolchain-setup.sh)
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/build.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  after_script:
    - export CCACHE_DIR="$PWD/ccache"
    - ccache --show-stats
  extends:
    - .linux-jobs
    - .send-email-on-failure
  cache:
    key: $TOOLCHAIN_URL
    paths:
      - toolchain.tar.xz
      - ccache
  artifacts:
    paths:
      - out/kernel/Image*
      - out/kernel/System.map
      - out/kernel/vmlinux
      - out/kernel/kernel.config
      - out/kernel/modules.tar.xz
      - out/kernel/dtbs.tar.xz
      - out/kernel/parameters.env

## TODO generate the Linux build jobs from a config file

# Linux ARM build
build:linux-arm:
  extends:
    - .linux-build
  variables:
    ARCH: arm
    KERNEL_CONFIG: multi_v7_defconfig
    TOOLCHAIN_URL: "$ARM_TOOLCHAIN"

# Linux ARM64 build
build:linux-arm64:
  extends:
    - .linux-build
  variables:
    ARCH: arm64
    KERNEL_CONFIG: defconfig
    TOOLCHAIN_URL: "$ARM64_TOOLCHAIN"

# Generic file generation job template
.generate-file:
  interruptible: true
  variables: !reference [.linux_testimage_resources, variables]
  stage: generate
  extends:
    - .linux-jobs
    - .lava-jobs
    - .send-email-on-failure
  variables:
    GIT_STRATEGY: none

# Fill the QcomLT ramdisk with kernel modules
generate:ramdisk-arm64:
  extends:
    - .generate-file
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/generate-ramdisk.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  needs: ["build:linux-arm64"]
  artifacts:
    paths:
      - out/ramdisk.env
  cache:
    paths:
      - configs

# Generate a bootimage per board with the ramdisks
generate:bootimg-arm64:
  extends:
    - .generate-file
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/generate-bootimg.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  parallel:
    matrix:
      # TODO this should be taken from a config file, including the per-board mkbootimg options
      - MACHINE:
          - apq8016-sbc
          - apq8096-db820c
          - sdm845-db845c
  needs: ["build:linux-arm64", "generate:ramdisk-arm64"]
  artifacts:
    paths:
      - out/$MACHINE/bootimg.env
  cache:
    paths:
      - mkbootimg

# Generate LAVA jobs to run with the bootimages
generate:lava-jobs:
  extends:
    - .generate-file
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/generate-lava-jobs.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  parallel:
    matrix:
      # TODO this should be taken from a config file, including the per-board tests and LAVA options
      - MACHINE:
          - apq8016-sbc
          - apq8096-db820c
          - sdm845-db845c
  needs: ["build:linux-arm64", "generate:bootimg-arm64", "generate:ramdisk-arm64"]
  artifacts:
    paths:
      - out/$MACHINE/lava/

# Submit LAVA jobs to SQUAD and setup callback URL to start "test:report" job when finished
test:submit:
  interruptible: true
  variables: !reference [.linux_testimage_resources, variables]
  stage: test
  before_script:
    # Build callback URL for the "report" job
    - apt-get install -y jq
  script:
    - |
      curl --silent --header "PRIVATE-TOKEN: $CI_API_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs?per_page=100" > jobs-manual.json
      job_id="$(jq -r ".[] | select(.name == \"$CI_REPORT_JOB_NAME\") | .id" jobs-manual.json)"
      export CALLBACK_URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/${job_id}/play"
      echo "$CI_REPORT_JOB_NAME job callback URL: $CALLBACK_URL"
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/test-submit.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
    - .linux-jobs
    - .lava-jobs
    - .send-email-on-failure
  needs: ["generate:lava-jobs"]
  variables:
    GIT_STRATEGY: none
    CI_REPORT_JOB_NAME: test:report
  environment:
    name: squad/$CI_COMMIT_REF_SLUG
    url: ${QA_SERVER}/${QA_SERVER_GROUP}/${QA_SERVER_PROJECT}/build/${CI_PIPELINE_ID}
    on_stop: test:stop
    auto_stop_in: 1 day

# Stop SQUAD build after a timeout or manually from UI
test:stop:
  variables: !reference [.linux_testimage_resources, variables]
  stage: test
  when: manual
  before_script:
    - apt-get install -y jq
  script:
    - squad_build_id="$(curl --silent "${QA_SERVER}/api/builds/?version=${CI_PIPELINE_ID}" | jq -r '.results[0].id')"
    - |
      curl --silent -X POST "$QA_SERVER/api/builds/$squad_build_id/cancel/" -H "Authorization: Token $CI_SQUAD_TOKEN"
  extends:
    - .linux-jobs
    - .lava-jobs
  variables:
    GIT_STRATEGY: none
  environment:
    name: squad/$CI_COMMIT_REF_SLUG
    action: stop

# Manual job triggered by Squad callback URL when finished
test:report:
  variables: !reference [.linux_testimage_resources, variables]
  allow_failure: false
  stage: report
  when: manual
  before_script:
    - apt-get install -y jq
  script:
    - squad_build_id="$(curl --silent "${QA_SERVER}/api/builds/?version=${CI_PIPELINE_ID}" | jq -r '.results[0].id')"
    - curl --silent "${QA_SERVER}/api/builds/${squad_build_id}/testjobs/?format=json" > test_jobs.json
  variables:
    GIT_STRATEGY: none
  extends:
    - .linux-jobs
    - .lava-jobs
  needs: ["generate:lava-jobs"]
  artifacts:
    paths:
      - test_jobs.json

# Parse the JSON report from SQUAD
test:final:
  variables: !reference [.linux_testimage_resources, variables]
  allow_failure: true
  stage: report
  before_script:
    - apt-get install -y jq
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/squad-json-to-junit.sh) > tests.xml
    - |
      jq -r '.results | .[] | .name, .environment, .external_url, .job_status | @sh' test_jobs.json \
        | xargs printf '------------\nname: %s\ndevice: %s\nurl: %s\nstatus: %s\n' \
        | tee email.txt
      if grep -qi "incomplete" email.txt; then
        echo "ERROR: Incomplete test job reported: exit code 1"
        exit 1
      fi
  needs: ["test:report"]
  variables:
    GIT_STRATEGY: none
  environment:
    name: squad/$CI_COMMIT_REF_SLUG
    action: stop
  artifacts:
    when: always
    paths:
      - tests.xml
    reports:
      junit: tests.xml
  extends:
    - .linux-jobs
    - .lava-jobs
    - .send-email-always

## TODO we should use Artifatcs instead of generic package storage
# Remove pipeline's artifacts from generic package storage
test:cleanup:
  variables: !reference [.linux_testimage_resources, variables]
  allow_failure: true
  stage: report
  before_script:
    - apt-get install -y jq
  script:
    - |
      curl --header "PRIVATE-TOKEN: $CI_API_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages?package_name=bootimg-${CI_PIPELINE_ID}" > packages.json
      IDS=$(cat packages.json | jq -r '.[] | .id')
      for id in ${IDS}; do
        curl --request DELETE --header "PRIVATE-TOKEN: $CI_API_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${id}"
      done
  extends:
    - .linux-jobs
    - .lava-jobs
  needs: ["test:report"]
  variables:
    GIT_STRATEGY: none
