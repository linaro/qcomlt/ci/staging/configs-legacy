# Migration plan

Describes phases and tasks to migrate `lt-qcom` [jenkins pipelines](https://ci.linaro.org/view/qclt/) to [codelinaro](https://git.codelinaro.org/).

## Main Phases & Tasks

1. jenkins using `git.codelinaro.org`
	1. git: migrate `https://git.linaro.org/landing-teams/working/qualcomm` to `git.codelinaro.org`
	2. jenkins: adjust `lt-qcom` jenkins jobs to *pull & push* to `git.codelinaro.org`
2. codelinaro fully replaces jenkins
	1. codelinaro: migrate staging `configs` repository to production
	2. codelinaro: enable scheduled pipelines
	3. codelinaro: monitor jenkins & codelinaro pipeline results
	4. jenkins: disable jenkins pipelines

## Phase 1: jenkins using `git.codelinaro.org`

The goal is to adjust current `lt-qcom` jenkins pipelines to fully use repositories at `git.codelinaro.org` (pull and push).
The latter implies that devs should start using the latter as main repositories (main git-remotes) and get used to it. Any codelinaro
issues would be handle at this point.

### Task 1.1 git: migrate `https://git.linaro.org/landing-teams/working/qualcomm` to `git.codelinaro.org`

The following repositories are used by jenkins CI; these are just a subset of https://git.linaro.org/landing-teams/working/qualcomm/ so the rest
should be migrated separately or all if desired.

- Private repositories

    - ssh://git@dev-private-git.linaro.org/landing-teams/working/qualcomm/sectools.git

- Public Pull-Push repositories

	- ssh://git.linaro.org/landing-teams/working/qualcomm/kernel-integration.git
	- ssh://git.linaro.org/landing-teams/working/qualcomm/kernel.git'

- Public

	- https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git
	- https://git.linaro.org/landing-teams/working/qualcomm/configs.git
	- https://git.linaro.org/landing-teams/working/qualcomm/automerge-rrcache.git
	- https://git.linaro.org/landing-teams/working/qualcomm/kernel.git
	- https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
	- https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
	- https://git.linaro.org/landing-teams/working/qualcomm/lk.git
	- https://git.linaro.org/landing-teams/working/qualcomm/partioning_tool.git
	- https://git.linaro.org/landing-teams/working/qualcomm/signlk.git
	- https://git.linaro.org/landing-teams/working/qualcomm/abl.git
	- https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git
	- https://git.linaro.org/power/automerge.git
	- https://git.linaro.org/landing-teams/working/qualcomm/configs.git
	- https://git.linaro.org/landing-teams/working/qualcomm/automerge-rrcache.git
	- https://git.linaro.org/landing-teams/working/qualcomm/abl.git
	- https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git
	- https://git.linaro.org/landing-teams/working/qualcomm/abl.git
	- https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git

### Task 1.2 jenkins: adjust `lt-qcom` jenkins jobs to *pull & push* to `git.codelinaro.org`

## Phase 2: codelinaro fully replaces jenkins

### Task 2.1. codelinaro: migrate staging `configs` repository to production

Sub-tasks:

1. Replace all git.linaro.org references to git.codelinaro.org
2. Push configs to production
3. Set CI variables
4. Trigger builds individually (and check results)
   - bootloader
   - linux-automerge
   - linux-testimages
   - oe
5. Trigger polls individually (and check results)
   - bootloader-poll
   - linux-automerge
   - oe-dunfell-poll
   - oe-honister-poll
   - oe-master-poll

### Task 2.2 codelinaro: enable scheduled pipelines

### Task 2.3 codelinaro: monitor jenkins & codelinaro pipelines

At this point, Jenkins and codelinaro should produce the same results (same pipeline status,
same log, same artifacts, etc.) so monitor these for *some time*.
   
### Task 2.4 jenkins: disable jenkins pipelines

