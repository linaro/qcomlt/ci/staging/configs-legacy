# Linaro Qualcomm Landing CI

Repository that host pipeline configurations (where their definitions are written in `yaml` format and
commonly called *configs* for Pipeline Configurations, following the **pipeline as code** practice )
for the Linaro Qualcomm Landing team.

## Single repository, multiple configs

This repository hosts multiple configs, each targeted for different purposes, e.g. some
pipelines are OpenEmbedded others kernel related. Normally, a gitlab project has its own
config file i.e. `.gitlab-ci.yml`, where for each git-push on any branch, a pipeline may be created
and executed; however  this is **not** the case for this repository, because pipelines do not belong to a
particular project (except for the `externals` pipelines), so instead of triggering these by
a git-push event (asynchronous), all pipelines are [scheduled](https://git.codelinaro.org/linaro/qcomlt/ci/configs/-/pipeline_schedules)

## Pipeline configuration types

There are three types of pipelines, located in separate folders:

- `polls`: polling pipelines, looking for project(s) updates and if there are, trigger one
  `builds` pipelines, e.g. `polls/.linux-automerge-poll.yml` polls different repos (`kernel`,
  `rrcache` and `configs`) for certain changes, e.g. new kernel tags, new head digests, etc.
- `builds`: pipelines where the real testing happens, and these are triggered by `poll`
   pipelines
- `externals`: pipelines that are consumed by other projects, e.g. `kernel`, so these are not
  triggered by polls jobs but by pull changes on the corresponding external repositories.

## Pipeline status email

In case of job failure, and email reporting the failure is sent to main developers. Also, when a non-poll
pipeline (builds and externals) executes succesfully, the latter status is also sent through email.
